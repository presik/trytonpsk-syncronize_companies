# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval

# 'InvoicesStart'

_ZERO = Decimal('0.00')


def get_shops_user():
    User = Pool().get('res.user')
    user = User(Transaction().user)
    shops = [s.id for s in user.shops]
    return shops


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'


class SaleIncomeDailyStart(metaclass=PoolMeta):
    __name__ = 'sale_pos.sale_income_daily.start'

    @classmethod
    def __setup__(cls):
        super(SaleIncomeDailyStart, cls).__setup__()
        cls.shop.domain.append(
            ('id', 'in', Eval('context', {}).get('shops', -1))
        )


# class SaleDailyReport(metaclass=PoolMeta):
#     __name__ = 'sale_goal.sale_daily_report'
#
#     @classmethod
#     def get_context(cls, records, header, data):
#         # report_context = super(SaleDailyReport, cls).get_context(records, data)
#         report_context = {}
#         pool = Pool()
#         Sale = pool.get('sale.sale')
#         User = pool.get('res.user')
#         Company = pool.get('company.company')
#         company = Company(data['company'])
#         Journal = pool.get('account.statement.journal')
#         user_ = User(Transaction().user)
#
#         dom_sales = [
#             ('company', '=', data['company']),
#             ('sale_date', '=', data['sale_date']),
#             ('number', '!=', None),
#         ]
#         states_sale = ['confirmed', 'processing', 'done']
#         if data['include_canceled']:
#             states_sale.append('cancel')
#         dom_sales.append(('state', 'in', states_sale))
#
#         journal_name = ''
#         if data['journal']:
#             dom_sales.append(('payments.statement.journal', '=', data['journal']))
#             journal = Journal(data['journal'])
#             journal_name = journal.name.upper()
#
#         ctx = {
#             'company': user_.company.id,
#             'user': user_.id,
#             'shops': [s.id for s in user_.shops],
#             'shop': user_.shop.id,
#             }
#         with Transaction().set_context(ctx):
#             sales = Sale.search(dom_sales, order=[('number', 'ASC')])
#
#         untaxed_amount_ = []
#         tax_amount_ = []
#         total_amount_ = []
#         total_amount_ = []
#         total_journal_paid_ = []
#         sequence = 0
#
#         for sale in sales:
#             sequence += 1
#
#             setattr(sale, 'sequence', sequence)
#             journal_paid = _ZERO
#             if data['journal'] and sale.state != 'cancel':
#                 for p in sale.payments:
#                     if p.statement.journal.id == data['journal']:
#                         journal_paid += p.amount
#                 total_journal_paid_.append(journal_paid)
#
#             setattr(sale, 'journal_paid', journal_paid)
#             if not hasattr(sale, 'sale_device'):
#                 setattr(sale, 'sale_device', None)
#             if sale.state == 'cancel':
#                 continue
#
#             untaxed_amount_.append(sale.untaxed_amount)
#             tax_amount_.append(sale.tax_amount)
#             total_amount_.append(sale.total_amount)
#
#         report_context['data'] = data
#         report_context['records'] = sales
#         report_context['total_amount'] = sum(total_amount_)
#         report_context['tax_amount'] = sum(tax_amount_)
#         report_context['untaxed_amount'] = sum(untaxed_amount_)
#         report_context['date'] = data['sale_date']
#         report_context['company'] = company.party.name
#         report_context['journal'] = journal_name
#         report_context['total_journal_paid'] = sum(total_journal_paid_)
#         return report_context


# class SaleMonthByShopStart(metaclass=PoolMeta):
#     __name__ = 'sale_goal.sale_month_shop.start'
#
#     @classmethod
#     def __setup__(cls):
#         super(SaleMonthByShopStart, cls).__setup__()
#         cls.shop.domain.append(
#             ('id', 'in', Eval('context', {}).get('shops', -1))
#         )


class SaleGoalAnnualRankingStart(metaclass=PoolMeta):
    __name__ = 'sale_goal.annual_ranking.start'

    @classmethod
    def __setup__(cls):
        super(SaleGoalAnnualRankingStart, cls).__setup__()
        cls.shop.domain.append(
            ('id', 'in', Eval('context', {}).get('shops', -1))
        )


class SaleDetailedStart(metaclass=PoolMeta):
    __name__ = 'sale_pos.sale_detailed.start'

    @classmethod
    def __setup__(cls):
        super(SaleDetailedStart, cls).__setup__()
        cls.shop.domain.append(
            ('id', 'in', Eval('context', {}).get('shops', -1))
        )


class SaleDetailedReport(metaclass=PoolMeta):
    __name__ = 'sale_pos.report_sale_detailed'

    @classmethod
    def get_context(cls, records, header, data):
        # report_context = super(SaleDetailedReport, cls).get_context(records, data)
        report_context = {}
        pool = Pool()

        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Company = pool.get('company.company')

        User = pool.get('res.user')
        user_ = User(Transaction().user)
        ctx = {
            'company': user_.company.id,
            'user': user_.id,
            'shops': [s.id for s in user_.shops],
            'shop': user_.shop.id,
            }

        sales_line_dom = [
            ('sale.sale_date', '>=', data['start_date']),
            ('sale.sale_date', '<=', data['end_date']),
            ('sale.company', '=', data['company']),
            ('sale.state', 'in', ['processing', 'done']),
        ]
        if data['salesman']:
            sales_line_dom.append(
                ('sale.salesman', '=', data['salesman'])
            )
        if data['shop']:
            sales_line_dom.append(
                ('sale.shop', '=', data['shop'])
            )
        if data['party']:
            sales_line_dom.append(
                ('sale.party', '=', data['party'])
            )
        if data['product']:
            sales_line_dom.append(
                ('product', '=', data['product'])
                )

        with Transaction().set_context(ctx):
            start_lines = SaleLine.search(sales_line_dom, order=[('sale.sale_date', 'ASC')])

        lines = []
        amount = []
        amount_w_tax = []
        for line in start_lines:
            if line.type == 'line' and not line.product:
                Sale.raise_user_error('sale_not_product', line.sale.number)

            is_invoiced = False
            if line.sale.invoices:
                for inv in line.sale.invoices:
                    if inv.state not in ['draft', 'cancel']:
                        is_invoiced = True
                if is_invoiced:
                    lines.append(line)
                    amount.append(line.amount)
                    amount_w_tax.append(line.amount_w_tax)
            else:
                lines.append(line)
                amount.append(line.amount)
                amount_w_tax.append(line.amount_w_tax)

        report_context['data'] = data
        report_context['amount'] = sum(amount)
        report_context['amount_w_tax'] = sum(amount_w_tax)
        report_context['records'] = lines
        report_context['company'] = Company(data['company'])
        return report_context


class ShopDailySummaryStart(metaclass=PoolMeta):
    __name__ = 'sale_pos.shop_daily_summary.start'

    @classmethod
    def __setup__(cls):
        super(ShopDailySummaryStart, cls).__setup__()
        cls.shop.domain.append(
            ('id', 'in', Eval('context', {}).get('shops', -1))
        )


class PortfolioDetailedReport(metaclass=PoolMeta):
    __name__ = 'party.portfolio_detailed.report'

    @classmethod
    def get_domain_invoice(cls, domain, data):
        shops = get_shops_user()
        dom = [('shop', 'in', shops)]
        domain.extend(dom)
        return domain


class ExpiredPortfolioReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.expired_portfolio_report'

    @classmethod
    def get_domain_invoice(cls, data):
        domain = super(ExpiredPortfolioReport, cls).get_domain_invoice(data)
        shops = get_shops_user()
        if shops:
            dom = [('shop', 'in', shops)]
            domain.extend(dom)
        return domain


# class InvoicesStart(metaclass=PoolMeta):
#     __name__ = 'invoice_report.print_invoices.start'
#
#     @classmethod
#     def __setup__(cls):
#         super(InvoicesStart, cls).__setup__()
#         cls.shop.domain.append(
#             ('id', 'in', Eval('context', {}).get('shops', -1))
#         )


class PortfolioBySalesmanReport(metaclass=PoolMeta):
    __name__ = 'sale_salesman.portfolio_by_salesman.report'

    # Overwrite method what generated domain of invoices
    @classmethod
    def get_domain_invoice(cls, domain, data):
        domain = super(PortfolioBySalesmanReport, cls).get_domain_invoice(
            domain, data)
        shops = get_shops_user()
        if shops:
            domain.append(('shop', 'in', shops))
        return domain


class MovesInvoicesReport(metaclass=PoolMeta):
    __name__ = 'account_invoice.moves_report'

    @classmethod
    def get_domain_invoice(cls, data):
        dom_invoices = super(MovesInvoicesReport, cls).get_domain_invoice(data)
        shops = get_shops_user()
        if shops:
            dom_invoices.append(('shop', 'in', shops))
        return dom_invoices
