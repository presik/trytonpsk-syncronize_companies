# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Bool, Eval


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    sincronize_companies = fields.Boolean('Syncronize Companies')
    connection_companies = fields.One2Many('company.connection_companies',
        'company', 'Connection Companies', states={
            'invisible': ~Bool(Eval('sincronize_companies')),
        })


class ConnectionCompanies(ModelSQL, ModelView):
    "Connection Companies"
    __name__ = 'company.connection_companies'
    database = fields.Char('Database', required=True)
    api_connection = fields.Char('Api Connection', required=True)
    company = fields.Many2One('company.company', 'Company', select=True,
        ondelete='CASCADE')
